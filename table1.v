// A simple log table for a 31 bit input
// The input is indx, and the output is tv (A 32 bit value)
// the output format is 8.24 (24 bits below the binary point)
//

module table1(indx,tv);

input [4:0] indx;
output [31:0] tv;
reg [31:0] _tv;
assign tv=_tv;

always @(*) begin
    case(indx)
      0: _tv=32'h4d104d;
      1: _tv=32'h9a209a;
      2: _tv=32'he730e7;
      3: _tv=32'h1344135;
      4: _tv=32'h1815182;
      5: _tv=32'h1ce61cf;
      6: _tv=32'h21b721c;
      7: _tv=32'h268826a;
      8: _tv=32'h2b592b7;
      9: _tv=32'h302a304;
      10: _tv=32'h34fb351;
      11: _tv=32'h39cc39f;
      12: _tv=32'h3e9d3ec;
      13: _tv=32'h436e439;
      14: _tv=32'h483f486;
      15: _tv=32'h4d104d4;
      16: _tv=32'h51e1521;
      17: _tv=32'h56b256e;
      18: _tv=32'h5b835bb;
      19: _tv=32'h6054609;
      20: _tv=32'h6525656;
      21: _tv=32'h69f66a3;
      22: _tv=32'h6ec76f0;
      23: _tv=32'h739873e;
      24: _tv=32'h786978b;
      25: _tv=32'h7d3a7d8;
      26: _tv=32'h820b826;
      27: _tv=32'h86dc873;
      28: _tv=32'h8bad8c0;
      29: _tv=32'h907e90d;
      30: _tv=32'h954f95b;
      31: _tv=32'hffff_ffff;
    endcase
end
endmodule
