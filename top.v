//
// This is a simple top level file for simulation of the log
// design during development (And maybe later)
//
`timescale 1ns/10ps

`include "table1.v"
`include "table2.v"
`include "log.v"

module top();

reg clk,rst;
reg pushin;
wire stopin;
reg [30:0] din;
wire pushout;
reg stopout;
wire [31:0] dout;

reg [6:0] fh,ft,fn;

reg [31:0] fifo[0:127];
reg [31:0] lexp;

integer rv1=0;

integer waitMax=0;

integer bomb=60;

reg debug=1;

initial begin
  fh=0;
  ft=0;
  pushin=0;
  rst=1;
  din=0;
  stopout=0;
  repeat(3) begin
    clk=0;
    #5;
    clk=1;
    #5;
  end
  rst=0;
  repeat(200000) begin
    clk=0;
    #5;
    clk=1;
    #5;
  end
  $display("Error --- Ran out of clocks");
  $finish;
end

initial begin
  if(debug) begin
    $dumpfile("log.vpd");
    $dumpvars(9,top);
  end
end


log l(clk,rst,pushin,din,stopin,pushout,dout,stopout);

task pushExp(input [31:0] a);
begin
  fifo[fh]=a;
  fh=fh+1;
  if(fh == ft) begin
    $display("Error --- You overran the fifo Morris");
    $finish;
  end
end
endtask

always @(posedge(clk)) begin
  #1;
  if(waitMax >0) begin
    if(rv1 == 0) begin
      rv1=$random&32'h0fffffff;
      rv1=rv1%waitMax;
      stopout=0;
    end else begin
      stopout=1;
      rv1=rv1-1;
    end
  end else stopout=0;
end


always @(posedge(clk)) begin
  if(pushout && ~stopout) begin
    if(pushout === 1'bx) begin
      $display("Error --- pushout is an x");
      #5 $finish;
    end
    if(fh==ft) begin
      $display("Error --- You pushed out, and I'm not expecting anything");
      #5;
      $finish;
    end else begin
      lexp = fifo[ft];
      ft=ft+1;
      if(lexp !== dout) begin
        $display("Error --- got %x expecting %x",dout,lexp);
        #5;
        $finish;
      end
      bomb=60;
    end
  end else begin
    if(fh != ft) bomb=bomb-1;
    if(bomb <= 0) begin
      $display("Error --- waited >50 clocks for output, none received");
      #5 $finish;
    end
  end
end


task dofile;
integer f;
reg [32:0] key;
reg [30:0] drv;
reg [31:0] exp;
integer junk;
reg alldone;
integer safety;
begin
  alldone=0;
  @(posedge(clk)) #1;
  f=$fopen("logCase1.txt","r");
  if(f == 0) begin
    $display("Error --- could not open test case file");
    $finish;
  end
  while(!$feof(f) && !alldone) begin
    junk=$fscanf(f,"%s %h %h",key,drv,exp);
    case(key)
      "q": begin
             alldone=1'b1;
           end
      "p": begin
             repeat(drv) @(posedge(clk)) #1;
           end
      "w": begin
             waitMax=exp;
           end
      "c": begin
             din=drv;
             if(drv != 0) pushExp(exp);
             pushin=1;
             @(posedge(clk));
             safety=50;
             while(stopin) begin
               @(posedge(clk));
               if(safety < 0) begin
                 $display("Error --- waited 50 clocks on a push, stopin high");
                 $finish;
               end
               safety=safety-1;
             end
             #1;
             din=24'ha5a5a5;
             pushin=0;
           end
      default:
        $display("%s in(%h) out(%h)",key,drv,exp);
    endcase
  end
  $fclose(f);
end
endtask

initial begin
#1;
while( rst == 1) @(posedge(clk)) #1;
repeat(3) @(posedge(clk)) #1;
dofile();
$display("\n\n\nOh what happiness, you completed the simulation\n\n");
$finish;
end




endmodule
