##
## DESCRIPTION

Improved the performance by increasing the operating frequence of Design at 250MHz further to 333 Mhz.
Modified the (log.v) problem so that it synthesizes correctly at 333 Mhz. 
Verified correct operation by running the test script(runlog.pl). 

##
## RUN COMMAND
./runlog.pl 4 results.txt

