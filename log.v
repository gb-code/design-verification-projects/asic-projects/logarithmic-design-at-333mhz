// This calculates a simple logrithmn
//

module log(clk,rst,pushin,din,stopin,pushout,dout,stopout);
input clk,rst;
input pushin;
input [30:0] din;
output stopin;
output pushout;
output [31:0] dout;
input stopout;

reg v,v_d;
reg recirculate;
reg [30:0] di,di_d;
reg [2:0] ll0,ll1,ll2,ll3,ll4,ll5,ll6,ll7;
reg [2:0] lm0,lm1,lh;

reg [4:0] highone;
reg [31:0] _dout;
reg [31:0] dw,dw1,dw2;
wire [31:0] part1;
wire [31:0] base;
wire [23:0] slope;
reg [23:0] slope1;
reg [31:0] wadj,mult_out1,mult_out3,mult_out2,wadj14,wadjm1,wadjm2;
reg [10:0] t2ix;
reg [30:0] din1;
reg [31:0] part4,part3,part2,part5,part6,part16,base1,base2,base3,base4;
wire pushout_5;
wire [38:0] mult_out;
reg pushout,pushout_1,pushout_2,pushout_3,pushout_4;//pushout_5 ;
assign pushout_5=v&(di != 0);
assign stopin =pushout&stopout;
assign dout = _dout;

//assign part
table1 t1(highone,part1);
table2 t2(t2ix,base,slope);
DW02_mult_2_stage #(15,24) s2 (.A(dw2[20:6]),.B(slope1),.TC(1'b0),.CLK(clk),.PRODUCT(mult_out));

function [2:0] lm4(input [3:0] a);
reg [2:0] res;
begin
  if(a == 0) res=0;
  else case(1)
    a[3]: res=3'b111;
    a[2]: res=3'b110;
    a[1]: res=3'b101;
    a[0]: res=3'b100;
  endcase
  lm4=res;
end
endfunction

always @(*) begin
  recirculate = v & stopout;
  v_d=pushin & ~stopin | v & stopout;
  if(recirculate) begin
    di_d = di;
  end else begin
    di_d = din1;
  end
  ll0=lm4(di[3:0]);
  ll1=lm4(di[7:4]);
  ll2=lm4(di[11:8]);
  ll3=lm4(di[15:12]);
  ll4=lm4(di[19:16]);
  ll5=lm4(di[23:20]);
  ll6=lm4(di[27:24]);
  ll7=lm4({1'b0,di[30:28]});
  lm0=lm4({ll3[2],ll2[2],ll1[2],ll0[2]});
  lm1=lm4({ll7[2],ll6[2],ll5[2],ll4[2]});
  lh=lm4({1'b0,1'b0,lm1[2],lm0[2]});
  highone[4]=lh[0];
  if(lh[0]) begin
      highone[3:2]=lm1[1:0];
      case(lm1[1:0])
        0: highone[1:0]=ll4[1:0];
        1: highone[1:0]=ll5[1:0];
        2: highone[1:0]=ll6[1:0];
        3: highone[1:0]=ll7[1:0];
      endcase
  end else begin
    highone[3:2]=lm0[1:0];
    case(lm0[1:0])
      0: highone[1:0]=ll0[1:0];
      1: highone[1:0]=ll1[1:0];
      2: highone[1:0]=ll2[1:0];
      3: highone[1:0]=ll3[1:0];
    endcase
  end
  dw = di << (31-highone); 
   
  t2ix = dw1[31:21];     
    
  mult_out2 = mult_out1 >> 15;
   
  _dout = part6-base4 +mult_out3;   
end

always @(posedge(clk) or posedge(rst)) begin
    if(rst) begin
     v <= 0;
        di <= 0;
    end else begin
          v <= #1 v_d;
        di <= #1 di_d;
    end
end

/*
FLIP FLOP controlling push model pipeline control signal
*/
always @(posedge(clk) or posedge(rst)) begin
    if(rst) begin
         pushout <= 0;
        pushout_1<=0;
        pushout_2 <= 0;
        pushout_3<=0;
        pushout_4<=0;
    end else begin
         pushout <= #1pushout_1;
       pushout_1 <= #1 pushout_2 ;
       pushout_2 <= #1pushout_3;
       pushout_3 <= #1pushout_4;
       pushout_4 <= #1pushout_5;
    end
end

/*
FLIP FLOP controlling intermediate signals in hardware for long combinatorial path
*/
always @(posedge(clk) or posedge(rst)) begin
    if(rst) begin
       
        dw1<=0;dw2<=0;
        mult_out1<= 0;mult_out3<=0;
        din1<= 0;

        part2<= 0;part3<= 0;part4<=0;part5<=0;
        base1<= 0;base2<= 0;base3<= 0;base4<= 0;
        
        slope1<=0;

        
    end else begin
  
        din1<= #1 din;
                
        part2<= #1 part1;part3<=#1 part2;part4<=#1part3;part5<=#1part4;part6 <= part5;           
        base1<= #1 base;base2<= #1base1;base3<=#1 base2;base4<=#1 base3;   
                
        slope1<= #1 slope;
        dw1<= #1 dw;dw2<= #1dw1;

        mult_out1 <= #1 mult_out;
        mult_out3<= mult_out2;

    end
end

endmodule



