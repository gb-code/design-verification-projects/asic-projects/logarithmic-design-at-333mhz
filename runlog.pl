#!/usr/bin/perl
@smod = ();
$sroute="";
@uname = ();
@uid = ();
$clkperiod = 4.0;

sub mkexfile{
  my $fn = shift;
  my $cmd = shift;
  open(fo,">",$fn) or die "\n\nFailed to create local command file $fn\n";
  printf fo "#!/usr/bin/csh\nsource /apps/design_environment.csh\n";
  printf fo "$cmd\n";
  close(fo);
  `chmod +x $fn`;
}

sub copyifneeded{
  my $fs=shift;
  my $fd=shift;
  return if( -e $fd );
  `cp $fs $fd`;
  die "Couldn't copy file $fs to current directory\n" if( not -e $fd) ;
}

sub compmod{
    my $fnx = shift;
    my $cname = shift;
    printf sf "read_verilog $cname\n";
    printf sf "check_design\n";
    printf sf "create_clock clk -name clk -period %f\n",$clkperiod*0.8;
    printf sf "set_propagated_clock clk\n";
    printf sf "set_clock_uncertainty 0.25 clk
set_output_delay %f -clock clk [all_outputs]
set_propagated_clock clk
compile_ultra
";
    printf sf "create_clock clk -name clk -period %f\n",$clkperiod;
    printf sf "set_propagated_clock clk\n";
    printf sf "set_clock_uncertainty 0.25 clk
    update_timing
"
}

sub comptable{
    my $fnx = shift;
    my $cname = shift;
    printf sf "read_verilog $cname\n";
    printf sf "check_design\n";
    printf sf "set_input_delay %f [all_inputs]\n",$clkperiod*0.1;
    printf sf "set_output_delay %f [all_outputs]\n",$clkperiod*0.8;    
    printf sf "compile_ultra\n";
}

sub makesynscript{
open(sf,">","synthesis.script") or die "\n\n\nFAILED --- Couldn't open the synthesis script for editing\n";
printf sf "set link_library {/apps/toshiba/sjsu/synopsys/tc240c/tc240c.db_NOMIN25 /apps/synopsys/I-2013.12-SP5/libraries/syn/dw_foundation.sldb }
set target_library {/apps/toshiba/sjsu/synopsys/tc240c/tc240c.db_NOMIN25}\n";
comptable(sf,"table1.v");
comptable(sf,"table2.v");
compmod(sf,"log.v");
printf sf "report -cell
report_timing -max_paths 10
write -format verilog -output log_gates.v
quit
";
close(sf);
}
sub runsynth{
mkexfile("lrcmd","dc_shell -f synthesis.script | tee synres.txt");
system("./lrcmd");
printf f "synthesis ran\n";
system("grep '(MET)' synres.txt")==0 or die "\n\n\n\nFAILED --- Didn't find timing met condition";
system("grep -i error synres.txt")!=0 or die "\n\n\n\nFAILED --- synthesis contained errors";
system("grep -i latch synres.txt")!=0 or die "\n\n\n\nFAILED --- synthesis created latches";
system("grep -i violated synres.txt")!=0 or die "\n\n\n\nFAILED --- synthesis violated timing";
system("grep -i 'timing arc' synres.txt")!=0 or die "\n\n\n\nFAILED --- timing arc seen";
$tline = `grep Total synres.txt`;
chomp($tline);
@gates = split(" ",$tline);
$size = @gates[3];
#printf f "Total gates %s\n", $size;
#die "\n\n\nFAILED --- Number of gates too small, check warinings\n\n" if ($size < 8000.0);
printf f "Design synthesized OK\n";
system("rm command.log");
system("rm default.svf");
print "\n\nSynthesis results are in file synres.txt\n";

}

sub runiverilog{
system("rm simres");
printf "Starting iverilog simulation... slow simulator, Be patient\n";
system("iverilog top.v DW02_mult_2_stage.v")==0 or die "\n\n\n\nFailed icarus Verilog compile\n";
system("./a.out | tee simres")==0 or die "\n\nFailed icarus verilog run\n";
printf f "icarus Verilog finished";
system("grep -i 'Oh what happiness, you completed the simulation' simres")==0 or die "\n\n\n\nFAILED --- iverilog simulation didn't get correct results";
printf f "icarus Verilog completed OK\n";
system("rm a.out");
}
sub runvcs {
printf f "vcs Simulation run on %s", `date`;
printf "Starting vcs simulation... Be patient\n";
system("rm simres");
mkexfile("lrcmd","rm -rf simv csrc simv.daidir\nvcs top.v DW02_mult_2_stage.v\nif (\$status != 0) exit \$status\n./simv | tee simres");
system("./lrcmd")==0 or die "\n\n\n\nFAILED --- vcs compile failed (Verilog problem)";
system("grep -i 'Oh what happiness, you completed the simulation' simres")==0 or die "\n\n\n\nFAILED --- simulation didn't get correct results";
printf f " simulation OK\n";
system("rm simres");
system("rm -rf simv simv.daidir csrc");

}
sub runncverilog{
mkexfile("lrcmd","rm simres\nncverilog top.v DW02_mult_2_stage.v| tee simres");
print "ncverilog simulation. Pretty fast";
printf f "NCverilog\n";
printf f "NCverilog Simulation run on %s", `date`;
printf "Starting NCverilog simulation... Fast simulator, large test cases... Be patient\n";
system("./lrcmd");
system("grep -i 'Oh what happiness, you completed the simulation' simres")==0 or die "\n\n\n Failed --- ncverilog didn't get correct results";
printf f " simulation OK\n";

}

($#ARGV >= 1 ) or die "Need the cycle time, and result name as parameters";
$clkperiod = $ARGV[0];
open(f,">",$ARGV[1]) or die "Couldn't open the output file";
$opt = "none";
$opt = $ARGV[2] if $#ARGV >=2 ;
copyifneeded("/home/morris/log/table1.v","table1.v");
copyifneeded("/home/morris/log/table2.v","table2.v");
copyifneeded("/home/morris/log/top.v","top.v");
copyifneeded("/home/morris/log/log.v","log.v");
copyifneeded("/home/morris/log/logCase1.txt","logCase1.txt");

printf f "Starting the run of the project cycle time %s\n",$ARGV[0] or die "\n\n\n\nFAILED --- didn't write\n\n\n";
printf f "iverilog Simulation run on %s", `date`;
printf f "%s on %s\n", $ENV{"USER"}, $ENV{"HOSTNAME"};
if ( $opt ne "synthesis") {
runiverilog;
runvcs;
runncverilog;
}

makesynscript;
runsynth;



printf f "%s\n",`uname -a`;
printf f "Completed %s\n",`date`;
$md5 = `cat runlog.pl top.v | md5sum`;
chomp($md5);
printf f "%s %s %s\n", $md5 , $ENV{"USER"}, $ENV{"HOSTNAME"};
printf f "%s\n",`id`;
printf f "Completed %s", `date`;
close f;
print "Successful Completion of HW run\n";
printf "Run summary file is %s\n",$ARGV[1];
